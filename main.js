const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const clear=document.getElementById("clear");
let spanSize=document.getElementById("size");
const increase=document.getElementById("increase");
const decrease=document.getElementById("decrease");
const colorBrush=document.getElementById("color");

colorBrush.addEventListener("input",function(){
    color=colorBrush.value
})

increase.addEventListener("click",function(){
    spanSize.textContent=size++;
})
decrease.addEventListener("click",function(){
    spanSize.textContent=size--;
})

clear.addEventListener("click",function(){
    ctx.clearRect(0,0,canvas.width,canvas.height);
})





    let size=20
    let color="black";
    let x;
    let y;
    isPressed=false;
     

function drawCircle(x,y){
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI * 2, true);
    ctx.fillStyle=color;
    ctx.fill()
}

function drawLine(x1,y1,x2,y2){
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y2);
    ctx.strokeStyle=color;
    ctx.lineWidth=size*2;
    ctx.stroke()
}


canvas.addEventListener("mousedown",(e)=>{
    isPressed=true;
    x=e.offsetX;
    y=e.offsetY;


})

canvas.addEventListener("mouseup",(e)=>{
    isPressed=false;
    x=undefined;
    y=undefined;

})

canvas.addEventListener("mousemove",(e)=>{
    if(isPressed){
        const x2=e.offsetX;
        const y2=e.offsetY;
      drawCircle(x2,y2)
      drawLine(x,y,x2,y2)
      x=x2;
      y=y2;
    }
})
